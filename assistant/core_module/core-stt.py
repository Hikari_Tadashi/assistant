import configparser, pyaudio, json
from vosk import Model, KaldiRecognizer

filename = ""

def readConfig():
    config = configparser.ConfigParser(allow_no_value=True)
    config.read('/home/chris/.config/assistant.conf')

    filename = config["core-stt.py"]["model"]

def runVosk():
    # This doesn't like a different directory, for some reason
    model = Model('model')
    rec = KaldiRecognizer(model, 16000)

    p = pyaudio.PyAudio()
    stream = p.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=8000)
    stream.start_stream()

    # loop forever
    while True:
        data = stream.read(4000)
        if len(data) == 0:
            # I don't want it to stop. Do I need to change this?
            break
        if rec.AcceptWaveform(data):
            # you can only get results once
            result = json.loads(rec.Result())
            if(result["text"] != ""):
                print(result["text"])
        #else:
            #print(rec.PartialResult())

    print(rec.FinalResult())
    
if __name__ == "__main__":
    readConfig()
    runVosk()
