import argparse

def setup_argparse():
    parser = argparse.ArgumentParser()

    parser.add_argument('-t',
                        dest='train',
                        help="start the training pipeline",
                        action='store_true')
    parser.add_argument('other',nargs=argparse.REMAINDER)
    args = parser.parse_args()
    return args

def read_config():
    config = configparser.ConfigParser(allow_no_value=True)
    config.read("/home/chris/.config/assistant.conf")

    return config

# in progress
def run_training_pipeline(config, data):
    pipeline = config["parser-pipeline"]

    for module in pipeline:
        process = subprocess.Popen(["python3",module,"-t"])      
    
def run_network_pipeline(config):
    print("Not yet implemented")

if __name__ == "__main__":
    config = read_config()
    args = setup_argparse()

    if args.train == True:
        run_training_pipeline()
    else:
        run_network_pipeline()

