# Is there any reason taht I shouldn't just merge this into core-server.py?

import configparser, subprocess

def readConfig():
    config = configparser.ConfigParser(allow_no_value=True)
    config.read("/home/chris/.config/assistant.conf")
    return config

if __name__ == "__main__":
    settings = readConfig()
    
    for program in settings['startup']:
        # remove python3 later, make them scripts.
        # this will not print their stderr, stdin, stdout
        subprocess.Popen(["python3",program])
