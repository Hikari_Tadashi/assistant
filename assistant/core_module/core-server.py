import configparser, socketserver, subprocess, multiprocessing
from multiprocessing import Process

class CoreHandler(socketserver.BaseRequestHandler):
    # just to see how this works
    def test(self, data):
        print(data)
    
    def handle(self):
        data = self.request[0].strip() #remove whitespace
        # I should make this set by config
        data = data.decode('utf-8')
        
        if("track" in data):
            print("passthrough")

        self.test(data)

def client_sent(data):
    subprocess.Popen(["python3", "core-client.py", data])
        
def spawn_processes(config):
    stdout = subprocess.STDOUT
    err = subprocess.PIPE

    # Each argument needs to be separate
    configFile = config['background']
    
    background = []
    for program in configFile:
        subprocess.run(["python3",program])
        
    while True:
        for process in background:
            stdout, stderr = process.communicate()
            if stdout == None:
                continue
            else:
                client_send(output)
 
def readConfig():
    config = configparser.ConfigParser(allow_no_value=True, strict=False)
    config.read("/home/chris/.config/assistant.conf")
    return config

def start_server(config):
    host = config['startup']['host']
    port = int(config['startup']['port'])
    print(host)
    print(port)
    
    with socketserver.UDPServer((host,port), CoreHandler) as server:
        server.serve_forever()
    
if __name__ == "__main__":
    config = readConfig()
    # Run background tasks
    background = Process(target=spawn_processes, args=(config,))
    background.start()
    # Start the listening server
    start_server(config)
