import datetime, sys, json, configparser

def read_config():
    config = configparser.ConfigParser(allow_no_value=True, strict=False)
    config.read("/home/chris/.config/assistant.conf")

    return config

# get the data that was piped in, and format it
def format_piped_data(data):
    data = json.loads(data)
    
    return data

def format_and_run(intentData):
    configData = read_config()
    configName = intentData.pop("INTENT_NAME")
    intentConfig = configData[configName]
    
    command = []
    # Does the command need a qualifier? python3, or a cmd name?
    if(intentConfig.get('command',False)):
        command.append(intentConfig['command'])

    for key in intentData:
        # if the command needs a prefix, add it
        if(intentConfig.get(key, None) != None):
            command.append(intentConfig[key])

        if (key == "date" or key == "time"):
            print("configuring datetime")
            #this is just for me
            if(key == "date"):
                command.append("30DEC20")
            else:
                command.append("18:00")
        else:
            command.append(intentData[key])

    #This is where the command would run. Does its stdin,stdout need to be tracked?
    print(command)

if __name__ == "__main__":
    data = (sys.stdin.read()).strip()
    intentData = format_piped_data(data)
    format_and_run(intentData)







