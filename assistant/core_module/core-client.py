import socket, sys

HOST, PORT = "localhost", 9999
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

data = "".join(sys.argv[1:])

sock.sendto(bytes(data,"utf-8"), (HOST, PORT))
print("Sent: ", data)
