import pathlib
import bracket_expansion as expand

def tokenize(sentence):
    """
    Converts a single sentence into a list of individual significant units
    Args:
        sentence (str): Input string ie. 'This is a sentence.'
    Returns:
        list<str>: List of tokens ie. ['this', 'is', 'a', 'sentence']
    """
    tokens = []

    class Vars:
        start_pos = -1
        last_type = 'o'

    def update(c, i):
        if c.isalpha() or c in '-{}':
            t = 'a'
        elif c.isdigit() or c == '#':
            t = 'n'
        elif c.isspace():
            t = 's'
        else:
            t = 'o'

        if t != Vars.last_type or t == 'o':
            if Vars.start_pos >= 0:
                token = sentence[Vars.start_pos:i].lower()
                if token not in '.!?':
                    tokens.append(token)
            Vars.start_pos = -1 if t == 's' else i
        Vars.last_type = t

    for i, char in enumerate(sentence):
        update(char, i)
    update(' ', len(sentence))
    return tokens

def resolve_conflicts(inputs, outputs):
    """
    Checks for duplicate inputs and if there are any,
    remove one and set the output to the max of the two outputs
    Args:
        inputs (list<list<float>>): Array of input vectors
        outputs (list<list<float>>): Array of output vectors
    Returns:
        tuple<inputs, outputs>: The modified inputs and outputs
    """
    data = {}
    for inp, out in zip(inputs, outputs):
        tup = tuple(inp)
        if tup in data:
            data[tup].append(out)
        else:
            data[tup] = [out]

    inputs, outputs = [], []
    for inp, outs in data.items():
        inputs.append(list(inp))
        combined = [0] * len(outs[0])
        for i in range(len(combined)):
            combined[i] = max(j[i] for j in outs)
        outputs.append(combined)
    return inputs, outputs

def load_sentences(config):
    training_data = {}

    directory = '/home/chris/Lab/assistant/assistant/skills'
    for path in pathlib.Path(directory).iterdir():
        skill = pathlib.Path(path).name
        for file in path.iterdir():
            if(pathlib.Path(file).suffix != ".intent"):
                continue
            else:
                intent = f"{skill}-{pathlib.Path(file).stem}"
                with file.open() as intentfile:
                    for line in intentfile:
                        line = tokenize(line)
                        expanded = expand.SentenceTreeParser(line).expand_parentheses()
                        for new_line in expanded:
                            joined = ""
                            for index,token in enumerate(new_line):
                                if index == 0:
                                    joined = token
                                else:
                                    joined += f" {token}"
                            if(training_data.get(intent) == None):
                                training_data[intent] = [joined.strip()]
                            else:
                                training_data[intent].append(joined.strip())
                                
    return training_data

def load_intents(intent_dir):
    intents = []
    path = pathlib.Path(intent_dir)
    
    for directory in path.iterdir():
        for intent in directory.iterdir():
            filename = intent.stem
            # Don't forget that .
            if intent.suffix == ".intent":
                intents.append(f"{directory.stem}-{filename}")
                
    return intents
