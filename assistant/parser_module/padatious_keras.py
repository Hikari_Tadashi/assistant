# I want to rename it padatious_tools
import parser_tools as tools
import pickle, argparse, configparser, id_manager, subprocess
import entity_keras

# Anywhere there is a pos_intents, it's entity_keras

def read_config():
    config = configparser.ConfigParser(allow_no_value=True,
                                       strict=False)
    config.read("/home/chris/.config/assistant.conf")
    return config

def setup_argparse():
    parser = argparse.ArgumentParser()

    parser.add_argument('-t',
                        dest='train',
                        help="start the training",
                        action='store_true')

    parser.add_argument('utterance',nargs=argparse.REMAINDER)

    args = parser.parse_args()
    return args

# This is called per intent
def train_intent_and_entity(intent,training_data):
    entities = set([])
    entity_edge_pair = []
    
    # a set of all tokens in intent training data, that starts w/ {
    for sentence in training_data[intent]:
        # I think it's expecting it to be tokenize here
        sentence = tools.tokenize(sentence)
        for token in sentence:
            if token.startswith('{'):
                # put the bracketless token in the entities holder
                key = token[0:len(token)]
                entities.add(key)

    # Train the edge_models
    processes = []
    for entity in entities:
        passed_data = {"entity":entity,
                       "intent":intent,
                       "training_data":training_data}

        # This will likely need to be changed
        with open('pickled.training', 'wb') as handle:
            pickle.dump(passed_data,handle)

        # This will have to be changed later as well
        process = subprocess.run(["python3",
                    "entity_keras.py",
                    "-t"])        

def train():
    # Do I need this?
    directory = '/home/chris/Lab/assistant/assistant/skills'
    intents = load_intents()
    training_data = load_sentences()

    # Does this ordering make sense? is it needed for training
    for intent in intents:
        train_intent_and_entity(intent,training_data)

def load_entities(config):
    entities = {}
    directory = '/home/chris/Lab/assistant/assistant/skills'

    for path in pathlib.Path(directory).iterdir():
        skill = pathlib.Path(path).name
        for file in path.iterdir():
            if(pathlib.Path(file).suffix != ".entity"):
                continue
            else:
                entity = f"{pathlib.Path(file).stem}"
                with file.open() as entityfile:
                    for line in entityfile:
                        line = line.strip()
                        line = tools.tokenize(line)
                        if(entities.get(entity) == None):
                            entities[entity] = [line]
                        else:
                            entities[entity].append(line)                            
    return entities

# This exists in one of the two kerases. Pull it out, put it here
def parse(utterance):
    intents = load_intents()
    final_intent_scores = []
    # entities = load_entities_and_edge_models()

    # Every intent checks for its own entities
    for intent in intents:
        final_intent_scores.append(calculate_intent_and_entities(utterance,intent))

    best_match = calculate_final_score(final_intent_scores)
    print(best_match)

def calculate_final_score(final_intent_scores):
    print("calculate_final_score not yet defined")

# This takes one intent, and finds all entity combinations.
# It then gets the best entity combination for that intent
def calculate_intent_and_entities(utterance,intent):
    possible_entity_matches = []

    possible_entity_matches.append(entity_keras.match(utterance,intent))
        
    # Get only the relavent matches
    # possible_entity_matches = [index for index in possible if conf >= 0.0]

    for match in possible_entity_matches:
        # conf = ranking entity sents against each other
        # conf = fancy_math()
        # adjusting the simple_intent to accound for entity matches
        # index.conf = math.sqrt(conf*simple_intent.match(index))

        print(match)
        # intent_scores.append(intent_keras(match))
        # adjust_and_compare_as_above()

    # self explainitory
    return possible_entity_matches

# This seems redundant, compared to load_sentences
def load_intents():
    intent_dir = "/home/chris/Lab/assistant/assistant/skills"
    intents = tools.load_intents(intent_dir)

    return intents

def load_data():
    data = {}
    data["ids"] = load_ids()
    data["sents"] = load_sentences()

def load_sentences():
    # structure is sentence_data = {intent:[],"sentences":[]}
    sentence_data = tools.load_sentences(config)
    
    return sentence_data

def load_ids():
    id_manager.load()

def get_best_match():
    print("Not yet implemented")
    
if __name__ == "__main__":
    config = read_config()
    args = setup_argparse()

    if args.train == True:
        train()
    else:
        data = ""
        for index,token in enumerate(args.utterance):
            if index == 0:
                data += token
            else:
                data += f" {token}"

        parse(data)
        
    
