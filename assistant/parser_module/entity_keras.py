# in-progress: What I am currently working on
import sys,re,pathlib,keras,id_manager,configparser,argparse,pickle
import tensorflow as tf
import numpy as np
from pathlib import Path
from keras import layers, activations
import parser_tools as tools
import bracket_expansion as expand

def read_config():
    config = configparser.ConfigParser(allow_no_value=True,
                                       strict=False)
    config.read("/home/chris/.config/assistant.conf")
    return config

def setup_argparse():
    parser = argparse.ArgumentParser()

    parser.add_argument('-t',
                        dest='train',
                        help="start the training",
                        action="store_true")

    parser.add_argument('utterance',nargs=argparse.REMAINDER)
    args = parser.parse_args()
    return args

def edge_verify(left_edge, right_edge):
    if left_edge > right_edge:
        return False

def load_ids(name=None):
    ids = {}
    
    if name != None:
        ids = id_manager.load(name)
    if ids == {}:
        ids = {":end":0}
        
    return ids

def save_ids(name,ids):
    id_manager.save(name,ids)

def get_end(sentence,direction):
    # if it's going right, the end is the length
    # if left, then -1
    if direction > 0:
        return len(sentence)
    else:
        return direction

# vectorize is directional. That's why it needs the position
def vectorize(sentence,position,ids,direction):
    vector = [0.0] * len(ids)
    end_position = get_end(sentence,direction)
    # whats the position for? doing id stuff?
    for index in range(position+direction,end_position,direction):
        # It looks like it is applying this to letters, rather than whole words...
        if sentence[index] in ids:
            vector = id_manager.assign(vector,ids,sentence[index], 1.0/abs(index-position))
            
    vector = id_manager.assign(vector,ids,":end", 1.0/abs(end_position-position))
    return vector

# this is expecting a direction, and a sentence.
def find_edge(direction,sentence,model,intent,entity):
    directory = "/home/chris/Lab/assistant/assistant/parser_module/entity_cache/"

    model = tf.keras.models.load_model(model)
    # fix. make it prettier
    ids = load_ids(f"{directory}{intent}.{entity}.ids")
    predictions = []

    # This needs to be centralized
    
    sentence = tools.tokenize(sentence)
    # This is doing tokens, as it should be
    # This is guessing the edge positions
    for position in range(len(sentence)):
        vector = vectorize(sentence,position,ids,direction)
        # It needs to be a double list for some reason...?
        vector = np.array([vector])
        predictions.append(model.predict(vector))
        print("Prediction made")

    # This ends up being a list, which means that it ultimately ends up being a double list/array
    return predictions

# Simulates multiple token words in adjacent entities
def pollute_edges(sentence,direction,entity_position,ids):
    bundled_training_data = []
    
    for current_position,token in enumerate(sentence):
        distance = current_position-entity_position
        # if we're next to the token, in the relavent direction
        if (int(distance > 0)-int(distance < 0) == direction) and token.startswith('{'):
            for pollution_length in range(1,4):
                polluted_sentence = sentence[:current_position]+[':0']*pollution_length+sentence[current_position+1:]
                polluted_position = entity_position+(pollution_length-1)*int(direction<0)
                print(f"Polluted sentence: {polluted_sentence}")
                
                pair = {"input":vectorize(polluted_sentence,polluted_position,ids,direction),"output":[1.0]}
                bundled_training_data.append(pair)
                print("Polluted sentence added")

    return bundled_training_data

def create_training_data(base_data,entity,direction,current_intent,ids):
    training_data = {"input":[],"output":[]}

    for intent in base_data.keys():
        for sentence in base_data[intent]:
            # This is doing the token matching, and this is why it keeps getting stuck at the beginning
            # This is poorly written. fix. in-progress
            sentence = tools.tokenize(sentence)
            for position, token in enumerate(sentence):
                sentence_vector = vectorize(sentence,position,ids,direction)
                training_data["input"].append(sentence_vector)
                print(f"does {entity}=={token} and {intent}=={current_intent}")
                if((entity == token)and(intent == current_intent)):
                    # if the token matches from a relavent sentence, it gets a 1.0
                    training_data["output"].append([1.0])

                    bundled_data = pollute_edges(sentence,direction,position,ids)
                    for pair in bundled_data:
                        training_data["input"].append(pair["input"])
                        training_data["output"].append(pair["output"])
                                                 
                else:
                    # if the token doesn't match, it gets 0.0
                    training_data["output"].append([0.0])

    return training_data
    
def build_and_train_model(bundled_training_data,ids):
        model = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(len(ids)),
                tf.keras.layers.Dense(3,activation="sigmoid"),
                tf.keras.layers.Dense(1,activation="sigmoid"),
            ]
        )

        early_stop = tf.keras.callbacks.EarlyStopping(monitor='loss',
                                                      mode='min',
                                                      baseline=0.4)
        # this is from the tf website
        model.compile(loss='binary_crossentropy', optimizer='rmsprop')
        callback = [early_stop]

        training_features = bundled_training_data["input"]
        training_labels = bundled_training_data["output"]

        training_features = np.array(training_features)
        training_labels = np.array(training_labels)

        model.fit(training_features,
                          training_labels,
                          epochs=1000,
                          verbose=0,
                          callbacks=callback)
        
        return model

# This is called once per entity, per intent.
def entity_finder(possible_match,model_pair,intent,key):
    possible_matches = [possible_match]
    new_match = possible_match
    edge_matches = {"left":[],"right":[]}

    edge_matches["left"].append(find_edge
                                (-1,new_match["sentence"],model_pair["left"],intent,key))
    edge_matches["right"].append(find_edge
                                 (+1,new_match["sentence"],model_pair["right"],intent,key))

    # for each valid edge_match pair.
    # I think this went through and got a val for each position. That seems wrong. How are there eq number?
    for left_edge_position in range(len(edge_matches["left"])):
        for left_edge_conf in edge_matches["left"][left_edge_position]:
            if left_edge_conf < 0.2:
                continue
            for right_edge_position in range(len(edge_matches["right"])):
                for right_edge_conf in edge_matches["right"][right_edge_position]:
                    if right_edge_conf < 0.2:
                        continue
                    # I didn't error check. This could be risky
                    if left_edge_position > right_edge_position:
                        continue
                    # if not is_valid(left_edge_position, right_edge_position):
                      #  continue

            # Get the entity in this spot
            extracted = new_match["sentence"][left_edge_position:right_edge_position+1]
            print(f"extracted: {extracted}")
            # I can replace the sentence here
            #new_match["new_sentence"] = replace(new_match["sentence"],
          #                                      extracted,
          #                                      entity)
                                
            #new_match["conf"] = (left_conf-0.5+right_conf-0.5)/2+0.5
            # Where am I getting this conf score?
            #new_match[f"{entity}-conf"] = (entity.match(extracted))
            #new_match[f"{entity}"] = extracted
            #possible_matches.append(new_match)
            
    return possible_matches

# It should train a single network
# I think base_data is the list of sentences for an entity
def train(base_data,intent,entity,config):
    directory = "/home/chris/Lab/assistant/assistant/parser_module/entity_cache/"
    # This is for human readability
    edge_networks,directions = {}, {"left":-1,"right":1}
    # Load the ids for this entity
    ids = load_ids(f"{directory}{intent}-{entity}.ids")
    # I don't like that this is hardcoded
    # in-progress. If it's a new ID dict, then populate it
    # This had a get_end directional aspect to it. I removed it. Is it needed? Might just have shortcut finding ids
    if len(ids) == 1:
        for sentence in base_data[intent]:
            sentence = tools.tokenize(sentence)
            for token in sentence:
                if token[0] != '{':
                    ids = id_manager.add_token(ids,token)
    save_ids(f"{directory}{intent}.{entity}.ids",ids)

    for sentence in base_data[intent]:
        sentence = tools.tokenize(sentence)
        for direction_name in directions.keys():
            print(f"Getting training data for {direction_name}")
            # I don't see where any edge stuff is happening in train
            direction_value = directions[direction_name]
            training_data = create_training_data(base_data,
                                                 entity,
                                                 direction_value,
                                                 intent,
                                                 ids)

            name = f"{intent}.{entity}.{direction_name}"
            model = build_and_train_model(training_data,ids)
            model.save(f"{directory}{name}.tf")

def match(sentence,intent,config=None):
    if config == None:
        config = read_config()
    directory = "/home/chris/Lab/assistant/assistant/parser_module/entity_cache/"
    entity_models = {}

    # Load the entity_models for this intent
    # This is hella convoluted, I don't like it
    # I had a better solution, moving this in in to padatious_keras. It would keep from having to load each file a thousand times
    for filename in Path(directory).iterdir():
        # Narrow it down to only entity_models for this intent
        if intent in str(filename) and filename.suffix == ".tf":
            # get the entity names from the trained NNs
            for suffix in filename.suffixes:
                if suffix.startswith(".{") and suffix[1:] not in entity_models.keys():
                    # This init is overwriting it
                    entity_models[suffix[1:]] = {"left":"","right":""}
            # Append the left and right edge models to the entity_models
            filename = str(filename)
            for key in entity_models.keys():
                if key in filename:
                    if "left" in filename:
                        entity_models[key]["left"] = filename
                    elif "right" in filename:
                        entity_models[key]["right"] = filename
    
    possible_matches = [{"sentence":sentence,
                      "conf":0.0}]

    # This is where the recursion happens, yeah? This is def duplicated
    for key in entity_models.keys():
        for possible_match in possible_matches:
            possible_matches = entity_finder(possible_match,entity_models[key],intent,key)

    # Do I need to do the max identification here?
    return possible_matches

# This will be taking, entity, intent, and a sentence_bundle?
if __name__ == "__main__":
    config = read_config()
    args = setup_argparse()

    # This definitely needs to be changed. Won't work decoupled
    with open('pickled.training', 'rb') as handle:
        training = pickle.load(handle)

    if args.train == True:
        train(training["training_data"],
                    training["intent"],
                    training["entity"],
                    config)
    else:
        utterance = ""
        for index,token in enumerate(args.utterance):
            if index == 0:
                utterance += token
            else:
                utterance += f" {token}"
        matches = match(utterance,intent,config)
        # I think I'm going to pipe this out as JSON
        print(matches)
