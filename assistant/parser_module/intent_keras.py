# KEY
# in-progress: what I am currently working on
from keras import layers, activations
import sys, re, pathlib, keras, configparser, id_manager, argparse, os
import bracket_expansion as expand
from pathlib import Path
import tensorflow as tf
import numpy as np
import parser_tools as tools

def read_config():
    config = configparser.ConfigParser(allow_no_value=True, strict=False)
    config.read("/home/chris/.config/assistant.conf")

    return config

def setup_argsparser():
    parser = argparse.ArgumentParser()

    parser.add_argument('-t',
                        dest='train',
                        help="start the training",
                        action='store_true')
    
    parser.add_argument('sentence',nargs=argparse.REMAINDER)
    args = parser.parse_args()
    return args

# next-steps. allow for save and recall
def generate_ids(sentences):
    ids = id_manager.load("none")
    
    ids['unknown_tokens'] = 0 #':0'
    # These are 'weights' generated by sentence length
    ids['w_1'] = 1 #':1'
    ids['w_2'] = 2 #':2'
    ids['w_3'] = 3 #':3'
    ids['w_4'] = 4 #':4'

    # This doesnt work cause it's not an object or dict
    for sentence_pair in sentences:
        tokens = tools.tokenize(sentence_pair[0])
        for token in tokens:
            ids = id_manager.add_token(ids, token)
    return ids

# Maybe this just needs to be sent the info?
def load_sents(config):
    return tools.load_sentences(config)

# this can be offloaded to id_manager.load()
def load_ids(config,sentences=None):
    if sentences == None:
        # Should this be tuples?
        sentences = load_sents(config)
    ids = generate_ids(sentences)
    return ids

# This scrapes the intent directory
def load_intents():
    tools.load_intents()

def generate_intent_networks():
    intents = load_intents()
    models = []
    
    for intent in intents:
        models.append(generate_network(intent))
    return models
        
def generate_network(intent):
    model = train(intent)
    return model

def match(sentence, models):    
    # the input already comes in vectorized
    sentence = np.array([sentence])
    predictions = {}
    
    for key in models:
        predictions[key] = models[key].predict(sentence)

    conf = 0.0
    for key in predictions:
        #if prediction.conf > conf:
         #   conf = prediction
        print(f"{key}: {predictions[key]}")

# This is meant for individual models, not all of the labels at once
# I need to improve the accuracy just a little bit
def build_model(training_data, ids):
    train_features = []
    train_labels = []
    
    for data,label in training_data:
        train_features.append(data)
        train_labels.append(label)
    
    model = tf.keras.Sequential(
        [
            tf.keras.layers.Dense(len(ids)),
            tf.keras.layers.Dense(10,activation="sigmoid"),
            tf.keras.layers.Dense(1,activation="sigmoid"),
        ]
    )    
    early_stop = tf.keras.callbacks.EarlyStopping(monitor='loss', mode='min', baseline=0.4)
    # this is generic from the keras/tensorflow website
    model.compile(loss='binary_crossentropy', optimizer='rmsprop')
    # callback must be a list
    callback = [early_stop]

    #adding numpy here. verify
    train_features = np.array(train_features)
    train_labels = np.array(train_labels)

    # This is based on the padatious neural network
    model.fit(train_features, train_labels, epochs=1000, verbose=0, callbacks=callback)
    return model

# make a "maybe" sentence
def pollute(sentence, position):
    # This needs to be updatable from a config file
    lenience  = 0.6
    polluted = []
    
    sentence = tools.tokenize(sentence) #should turn it into a list right?
    for _ in range(int((len(sentence) + 2) / 3)): #oh, i don't think this will work. it looks like it's supposed to yeild, not return
        sentence.insert(position, ':null:')
        # verify
        newsentence = ""
        for token in sentence:
            newsentence+=" "+token
        polluted.append((newsentence.strip(), lenience))
    return polluted
        
def vectorize(sentence, ids): #vectorize a sentence
    # This is the same in both
    vector = [0.0] * len(ids)
    # this is different
    unknown = 0

    # This is the tokenizer tool. It doesn't need to be in here. 
    tokens = tools.tokenize(sentence)
    for token in tokens:
        if token in ids:
            vector =id_manager.assign(vector,ids,token,1.0)
        else:
            unknown += 1

    # verify. 
    if len(sentence) > 0:
        vector = id_manager.assign(vector,ids,"unknown_tokens",unknown/float(len(sentence))) #verify. this works fine. straight from padatious
        vector = id_manager.assign(vector,ids,"w_1",len(sentence)/1)
        vector = id_manager.assign(vector,ids,"w_2",len(sentence)/2)
        vector = id_manager.assign(vector,ids,"w_3",len(sentence)/3)
        vector = id_manager.assign(vector,ids,"w_4",len(sentence)/4)

    return vector
        
# BOW for sents, one hot for tokens
def bundle_training_data(vector,ids,output):
    inputs = vectorize(vector, ids) # calling function vectorize
    # I think output needs to exist as a list for a specific reason, bu tI don't remember why
    outputs = [output]

    return (inputs,outputs)
    
# I think this way of defining weights preflattens the network
def weight(sent): # I dont see how this is impacting the network. verify
    tokens = tools.tokenize(sent)
    
    training_data = []
    def calc_weight(w):
        return pow(len(w), 3.0)
    total_weight = 0.0
    for token in tokens:
        total_weight += calc_weight(token)
    for token in tokens:
        weight = 0 if token.startswith('{') else calc_weight(token)
        # return wont work, since it outputs multiple. does this need to be a yeild?
        
        training_data.append((token, weight/total_weight))
    return training_data

def load_networks(filepath):
    path = pathlib.Path(filepath)
    networks = {}
    
    for model in path.iterdir():
        name = os.path.basename(model)
        networks[name] = tf.keras.models.load_model(model)

    return networks

# taken straight from padatious
def resolve_conflicts(training_data):
    sentences = []
    labels = []

    for sentence,label in training_data:
        sentences.append(sentence)
        labels.append(label)
    
    data = {}
    # I don't like how this looks. Is this right?
    for sentence, label in zip(sentences, labels):
        pair = tuple(sentence)
        if pair in data:
            data[pair].append(label)
        else:
            data[pair] = [label]

    sentences, labels = [], []
    for sentences, labels in data.items():
        sentences.append(list(sentences))
        combined = [0] * len(labels[0])
        for index in range(len(combined)):
            combined[index] = max(output[index] for label in labels)
        labels.append(combined)

    temporary_training = ()
    # zip is like, zipper in the sentences and labels
    for inp, out in zip(sentences, labels):
        temporary_training.append(sentence,label)
    
    return temporary_training

def save_models(models):
    for model,name in models :
        # This should reference the config file, or be strictly local
        model.save(f"neural-networks/{name}.tf")

#in-progress. this takes multiple sentences from the entity parser
def parse(sentence, config):
    ids = load_ids(config)    
    #for sentence in sentences:
    vector = vectorize(sentence, ids)
    network_directory = config['intent_keras.py']['network_dir']

    # this is a tuple, should probably be a dict actually
    models = load_networks(network_directory)
    match(vector,models)

def train(config):
   # This needs to be designed for multiprocessing
    network_directory = config['intent_keras.py']['network_dir']

    # fix. this is now duplicate data
    lenience = 0.6
    good = 1.0
    # This is a dictionary of lists
    intent_data_dict = load_sents(config)
    ids = load_ids(config,intent_data_dict)
    
    intents = intent_data_dict.keys()
    models = []

    # This first for loop is where the processes could be split up
    for intent in intent_data_dict:
        # The training data for this current intent. It refreshes after every model training
        training_data = []

        # add sentences that DO relate, to train what IS good
        print(f"building intent sentences for {intent}")
        for sentence in intent_data_dict:
            # I think I need to expand the sentences
            training_data.append(bundle_training_data
                                 (sentence,ids,good))
            pair = ()
            pair = weight(sentence)
            for token,token_weight in pair: 
                training_data.append(bundle_training_data
                                     (token,ids,token_weight))

                tokens = tools.tokenize(sentence)

            if not any(word[0] == ':' and word != ':' for word in tokens):
                output = pollute(sentence, 0)
                for sentence,label in output:
                    training_data.append(bundle_training_data
                                         (sentence,ids,label))
            
                output = pollute(sentence, len(sentence))
                for sentence,label in output:
                    training_data.append(bundle_training_data
                                         (sentence,ids,label))

        # add sentences that DONT relate, to train whats NOT good
        for key in intents:
            # skip the ones that are good
            if intent == key:
                continue
            for sentences in intent_data_dict[key]:
                for sentence in sentences:
                    training_data.append(
                        bundle_training_data(sentence,ids,0.0))
                    training_data.append(
                        bundle_training_data(':null:',ids,0.0))
                    training_data.append(
                        bundle_training_data("",ids,0.0))
                
        # This needs to actually be for sents, not the tagged data
        if sentence in intent:
            without_entities = sentence[:]
            for i, token in enumerate(without_entities):
                if token.startswith('{'):
                    without_entities[i] = ':null:'

            if sentence != without_entities:
                training_data.append(bundle_training_data
                                     (without_entities,ids,0.0))

        #training_data = resolve_conflicts(training_data)

        # This isn't displaying calender set trained
        model = build_model(training_data,ids)
        # name the intent with it
        models.append(tuple((model,intent)))

    return models
    
if __name__ == '__main__':    
    # init the program
    config = read_config()
    
    # it should either take a flag, or some kind of data
    args = setup_argsparser()

    # So... This is where I should break up the sentences to train, simply because I can break it off to optimize for multi-process, yeah? It would make sense the network module would handle its internal multiprocessing
    if args.train == True:
        models = train(config)
        save_models(models)
    else:
        data = ""
        for index,token in enumerate(args.sentence):
            if index == 0:
                data += token
            else:
                data += f" {token}"

        parse(data, config)
