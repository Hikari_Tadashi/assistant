import pickle
from pathlib import Path

def adj_token(token):
    if token.isdigit():
        for i in range(10):
            if str(i) in token:
                token = token.replace(str(i), '#')
    return token

def vector(ids):
    return [0.0] * len(ids)

def save(name,ids):
    # This could work fine in the long run
    with open(name,'wb') as handle:
        pickle.dump(ids,handle)

# This needs to be reworked
def load(name):
    # This is just for local reference
    ids = {}

    if name == None:
        return ids
    elif Path(name).exists():
        with open(name,'rb') as handle:
            ids = pickle.load(handle)
    else:
        print(f"no ids exists with name {name}")
            
    return ids

def assign(vector,ids,key,val):
    vector[ids[adj_token(key)]] = val

    return vector

def add_token(ids,token):
    token = adj_token(token)
    if token not in ids:
        ids[token] = len(ids)

    return ids

def add_sent(sent):
    for token in sent:
        add_token(token)
