import tensorflow as tf
from tensorflow import keras

import numpy as np
import matplotlib.pyplot as plt
import random

model = "neural-networks/network_0.tf"
converter = tf.lite.TFLiteConverter.from_saved_model(model)
tflite_model = converter.convert()

# Save the TF Lite model.
with tf.io.gfile.GFile('neural-networks/model.tflite', 'wb') as f:
  f.write(tflite_model)
